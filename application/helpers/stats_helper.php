<?php

if (!function_exists('remove_outliers')) {

    function remove_outliers($data, $float = 2) {
        $return = array();
        if (empty($data))
            return;
        if (!is_array($data))
            return;
//        $unique = array_unique($data);
//        sort($unique);
//        if (count($unique) > 2) {
//            array_shift($unique);
//            array_pop($unique);
//        }
//        $result = array_intersect($data, $unique);
//        $return += array('result' => number_format(array_sum($result) / count($result), $float));
//        $return += array('data' => $result);
//        return $return;
        sort($data);
        if (count($data) > 2) {
            array_shift($data);
            array_pop($data);
        }
        $return += array('result' => number_format(array_sum($data) / count($data), $float));
        $return += array('data' => $data);
        return $return;
    }

}

