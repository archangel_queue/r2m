<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function resize_image($realpath, $type = 'jpg') {
    $setwidth = 650;
    list($o_width, $o_height) = getimagesize($realpath);
    if ($o_width > $setwidth) {
        $ratio = $o_width / $o_height;
        // Resample
        $height = $setwidth / $ratio;
        $image_p = imagecreatetruecolor($setwidth, $height);
        switch ($type) {
            case 'jpeg':
                $image = imagecreatefromjpeg($realpath);
                break;
            case 'gif':
                $image = imagecreatefromgif($realpath);
                break;
            case 'png':
                $image = imagecreatefrompng($realpath);
                break;
            default:
                break;
        }
        if (!empty($image)) {
            imagecopyresampled($image_p, $image, 0, 0, 0, 0, $setwidth, $height, $o_width, $o_height);
            // Output
            switch ($type) {
                case 'jpeg':
                    imagejpeg($image_p, $realpath, 100);
                    break;
                case 'gif':
                    imagegif($image_p, $realpath);
                    break;
                case 'png':
                    imagepng($image_p, $realpath, 0);
                    break;
                default:
                    break;
            }
        }
    }
}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

