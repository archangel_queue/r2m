<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Thai {

    public function Month($month) {
        $thai_month = array(
            "1" => "มกราคม",
            "2" => "กุมภาพันธ์",
            "3" => "มีนาคม",
            "4" => "เมษายน",
            "5" => "พฤษภาคม",
            "6" => "มิถุนายน",
            "7" => "กรกฎาคม",
            "8" => "สิงหาคม",
            "9" => "กันยายน",
            "10" => "ตุลาคม",
            "11" => "พฤศจิกายน",
            "12" => "ธันวาคม"
        );
        return $thai_month[$month];
    }

    public function DayOfWeek($dow) {
        $thai_day = array("จันทร์",
            "อังคาร",
            "พุธ",
            "พฤหัสบดี",
            "ศุกร์",
            "เสาร์",
            "อาทิตย์"
        );
        return $thai_day[$dow];
    }

}
