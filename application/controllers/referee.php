<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Referee extends CI_Controller {

    private $LoginInfo = NULL;

    public function __construct() {
        parent::__construct();
        $this->output->set_header("Content-type:text/html; charset=utf-8");
        $login_id = $this->session->userdata('login_id');
        $login_type = $this->session->userdata('login_type');
        $this->LoginInfo = array('id' => $login_id, 'type' => $login_type);
    }

    public function scored1st($team_id = 0) {
        if (empty($this->LoginInfo['id']))
            redirect('/content/login');
        if (empty($team_id))
            redirect('/content/index');
        $data['team'] = $this->mydb->GetTeams($team_id);
        if (empty($data['team']))
            redirect('/content/index');
        $post = $this->input->post();
        if (!empty($post)) {
            $pointsupdate = $post['points'];
            $data_create = array();
            foreach ($pointsupdate as $key => $value) {
                if (isset($value) && strlen($value) > 0) {
                    $data_create[] = array(
                        'points_value' => (double) $value,
                        'points_subject' => $key,
                        'points_team' => $team_id,
                        'points_referee' => $this->LoginInfo['id'],
                    );
                }
            }
            // close update points after end of 1st season
            // $this->mydb->UpdatePoints($data_create);
            redirect('/referee/scored1st/' . $team_id);
        }
        $data['before'] = $this->mydb->GetTeamsID($team_id);
        $data['after'] = $this->mydb->GetTeamsID($team_id, 'after');
        $data['points'] = $this->mydb->GetPoints($team_id, $this->LoginInfo['id']);
        $data['elements'] = $this->mydb->GetFormA('%');
        $this->template->write_view('menu', 'revs/menu');
        $this->template->write_view('content', 'revs/form_bc_01', $data, TRUE);
        $this->template->render();
    }

    public function scored2st($team_id = 0) {
        if (empty($this->LoginInfo['id']))
            redirect('/content/login');
        if (empty($team_id))
            redirect('/content/index');
        $data['team'] = $this->mydb->GetTeams($team_id);
        if (empty($data['team']))
            redirect('/content/index');
        $post = $this->input->post();
        if (!empty($post)) {
            $pointsupdate = $post['points'];
            $data_create = array();
            foreach ($pointsupdate as $key => $value) {
                if (isset($value) && strlen($value) > 0) {
                    $data_create[] = array(
                        'points_value' => (double) $value,
                        'points_subject' => $key,
                        'points_team' => $team_id,
                        'points_referee' => $this->LoginInfo['id'],
                    );
                }
            }
            // close update points after end of 1st season
            $this->mydb->UpdatePointsB($data_create);
            redirect('/referee/scored2st/' . $team_id);
        }
        $data['before'] = $this->mydb->GetTeamsIDFinal($data['team'][0]->team_name);
        $data['after'] = $this->mydb->GetTeamsIDFinal($data['team'][0]->team_name, 'after');
        $data['points'] = $this->mydb->GetPointsB($team_id, $this->LoginInfo['id']);
        $data['elements'] = $this->mydb->GetFormB('%');
        $this->template->write_view('menu', 'revs/menu');
        $this->template->write_view('content', 'revs/form_bc_02', $data, TRUE);
        $this->template->render();
    }

}
