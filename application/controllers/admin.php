<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class admin extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->output->set_header("Content-type:text/html; charset=utf-8");
        //$this->output->enable_profiler(TRUE);
    }

    public function index() {
        $login_type = $this->session->userdata('login_type');
        if (empty($login_type))
            redirect('/content/login');
        $this->template->write_view('menu', 'admins/menu');
        $this->template->write_view('content', 'admins/index');
        $this->template->render();
    }

    public function round1st($id = '') {
        $login_type = $this->session->userdata('login_type');
        if (empty($login_type))
            redirect('/content/login');
        if (empty($id))
            redirect('/admin/teams');
        $data['teams'] = $this->mydb->GetTeams($id);
        if (empty($data['teams']))
            redirect('/admin/teams');
        $data['topics'] = $this->mydb->GetFormA('%');
        $data['revs'] = $this->mydb->GetRevs();
        $data['points'] = $this->mydb->GetPointsAll();
        $this->load->helper('stats');
        $this->template->write_view('menu', 'admins/menu');
        $this->template->write_view('content', 'admins/points-team', $data);
        $this->template->render();
    }

    public function revs1st($id = "") {
        $login_type = $this->session->userdata('login_type');
        if (empty($login_type))
            redirect('/content/login');
        if (empty($id))
            redirect('/admin/teams');
        $data['revs'] = $this->mydb->GetRevs($id);
        if (empty($data['revs']))
            redirect('/admin/teams');
        $data['topics'] = $this->mydb->GetFormA('%');
        $data['teams'] = $this->mydb->GetTeams('%');
        $data['points'] = $this->mydb->GetPointsAll();
        $this->load->helper('stats');
        $this->template->write_view('menu', 'admins/menu');
        $this->template->write_view('content', 'admins/points-rev', $data);
        $this->template->render();
    }

    public function points() {
        $login_type = $this->session->userdata('login_type');
        if (empty($login_type))
            redirect('/content/login');
        $data['teams'] = $this->mydb->GetTeams('%');
        $data['topics'] = $this->mydb->GetFormA('%');
        $data['revs'] = $this->mydb->GetRevs();
        $data['points'] = $this->mydb->GetPointsAll();
        //$this->load->view('admins/points-wide', $data);
        $this->load->helper('stats');
        $this->template->write_view('menu', 'admins/menu');
        $this->template->write_view('content', 'admins/points-wide', $data);
        $this->template->render();
    }

    public function teams() {
        $login_type = $this->session->userdata('login_type');
        if (empty($login_type))
            redirect('/content/login');
//        $post = $this->input->post();
//        $get = $this->input->get();
//        if (!empty($post)) {
//            // process
//            $team_id = !empty($post['team_id']) ? $post['team_id'] : 0;
//            $team_name = !empty($post['team_name']) ? $post['team_name'] : NULL;
//            if (!empty($team_name))
//                $this->mydb->UpdateTeams($team_name, $team_id);
//            redirect('/admin/teams');
//        }
//        if (!empty($get)) {
//            $team_id = !empty($get['team_id']) ? $get['team_id'] : 0;
//            $this->mydb->RemoveTeams($team_id);
//            redirect('/admin/teams');
//        }
        $data['teams'] = $this->mydb->GetTeamsJoinPointsAll('%');
        $this->template->write_view('menu', 'admins/menu');
        $this->template->write_view('content', 'admins/teams', $data);
        $this->template->render();
    }

    public function referee($id = 0) {
        $login_type = $this->session->userdata('login_type');
        if (empty($login_type))
            redirect('/content/login');
        if (empty($id))
            redirect('/admin/referees');
        $post = $this->input->post();
        if (!empty($post)) {
            if (empty($post['referee_name'])) {
                $this->session->set_flashdata('alert', array('text' => "กรุณากรอกชื่อกรรมการ", 'type' => 'danger', 'head' => 'พบข้อผิดพลาด'));
            }
            if (empty($post['referee_user'])) {
                $this->session->set_flashdata('alert', array('text' => "กรุณากรอกชื่อผู้ใช้", 'type' => 'danger', 'head' => 'พบข้อผิดพลาด'));
            }
            if (empty($post['referee_pwd'])) {
                $this->session->set_flashdata('alert', array('text' => "กรุณากรอกรหัสผ่าน", 'type' => 'danger', 'head' => 'พบข้อผิดพลาด'));
            }
            if (!empty($post['referee_name']) && !empty($post['referee_user']) && !empty($post['referee_pwd'])) {
                $this->mydb->UpdateLogin($post['referee_user'], $post['referee_pwd'], $post['referee_name'], 'referee', $id);
            }
            redirect('/admin/referee/' . $id);
        }
        $referees = $this->mydb->GetLogins('referee', $id);
        if (!empty($referees))
            $data['referee'] = $referees[0];
        else
            redirect('/admin/refrees');
        $this->template->write_view('menu', 'admins/menu');
        $this->template->write_view('content', 'admins/referee', $data);
        $this->template->render();
    }

    public function referees() {
        $login_type = $this->session->userdata('login_type');
        if (empty($login_type))
            redirect('/content/login');
        $post = $this->input->post();
        $get = $this->input->get();
        if (!empty($post)) {
            if (empty($post['referee_name'])) {
                $this->session->set_flashdata('alert', array('text' => "กรุณากรอกชื่อกรรมการ", 'type' => 'danger', 'head' => 'พบข้อผิดพลาด'));
            }
            if (empty($post['referee_user'])) {
                $this->session->set_flashdata('alert', array('text' => "กรุณากรอกชื่อผู้ใช้", 'type' => 'danger', 'head' => 'พบข้อผิดพลาด'));
            }
            if (empty($post['referee_pwd'])) {
                $this->session->set_flashdata('alert', array('text' => "กรุณากรอกรหัสผ่าน", 'type' => 'danger', 'head' => 'พบข้อผิดพลาด'));
            }
            if (!empty($post['referee_name']) && !empty($post['referee_user']) && !empty($post['referee_pwd'])) {
                $this->mydb->UpdateLogin($post['referee_user'], $post['referee_pwd'], $post['referee_name']);
            }
            redirect('/admin/referees');
        }
        $data['referees'] = $this->mydb->GetLogins();
        $this->template->write_view('menu', 'admins/menu');
        $this->template->write_view('content', 'admins/referees', $data);
        $this->template->render();
    }

    public function json() {
        $post = $this->input->post();
        $data = NULL;
        if(!empty($post)) {
            if(!empty($post['team_id']) && !empty($post['pass'])) {
                $data = $this->mydb->UpdateTeamFinal(array('f_team'=>$post['team_id']), $post['pass']);
            }elseif(!empty($post['team_id']) && empty($post['pass'])) {
                $data = $this->mydb->UpdateTeamFinal(array('f_team'=>$post['team_id']), $post['pass']);
            }
        }
        echo $data;
    }
}
