<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Content extends CI_Controller {

    public function __construct() {
        parent::__construct();
        //$this->output->enable_profiler(TRUE);
    }

    public function index() {
        $this->template->write('title', 'Monthly Logs');
        $login_id = $this->session->userdata('login_id');
        $login_type = $this->session->userdata('login_type');
        if (empty($login_type))
            redirect('/content/login');
        switch ($login_type) {
            case 'admin':
                $this->template->write_view('menu', 'admins/menu');
                $this->template->write_view('content', 'admins/index');
                break;
            case 'referee':
                redirect('/content/finish');
                break;
        }
        $this->template->render();
    }

    public function finish() {
        $login_id = $this->session->userdata('login_id');
        $login_type = $this->session->userdata('login_type');
        if (empty($login_type))
            redirect('/content/login');
        $this->template->write_view('menu', 'revs/menu');
        $data['teams'] = $this->mydb->GetTeamsJoinPointsFinish('%', array($login_id));
        $this->template->write_view('content', 'revs/final', $data);
        $this->template->render();
    }

    public function semi() {
        $login_id = $this->session->userdata('login_id');
        $login_type = $this->session->userdata('login_type');
        if (empty($login_type))
            redirect('/content/login');
        $this->template->write_view('menu', 'revs/menu');
        $data['teams'] = $this->mydb->GetTeamsJoinPoints('%', array($login_id));
        $this->template->write_view('content', 'revs/index', $data);
        $this->template->render();
    }

    public function login() {
        $this->template->write('title', 'เข้าสู่ระบบ R2M2014');
        $this->template->write_view('content', 'contents/login');
        $this->template->render();
    }

    public function auth() {
        $object = $this->mydb->GetLogIn($this->input->post('username'), $this->input->post('password'));
        if (!empty($object))
            $this->session->set_userdata((array) $object[0]);
        redirect('content/' . (!empty($object) ? 'index' : 'login'));
    }

    public function bootcamp01($team = "") {
        $this->template->write('title', 'Monthly Logs');
        $login_type = $this->session->userdata('login_type');
        if (empty($login_type))
            redirect('/content/login');
        $data['elements'] = $this->mydb->GetFormA('%');
        $this->template->write_view('content', 'revs/form_bc_01', $data, TRUE);
        $this->template->render();
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect('content');
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
