<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pdf extends CI_Controller {

    public function __construct() {
        parent::__construct();
        //$this->output->enable_profiler(TRUE);
    }

    public function index() {
        include_once FCPATH . '/fpdf/fpdf.php';
        $array_abc = array();
        $array_abc['object']['title'] = "a";
        $array_abc['a'][1] = 5;
        $array_abc['a'][2] = 5;
        $array_abc['a'][3] = 5;
        $array_abc['a'][4] = 5;
        $array_abc['a'][5] = 5;
        $array_abc['a'][6] = 5;
        $array_abc['a'][7] = 5;
        $array_abc['a'][8] = 20000;
        $array_abc['b'][1] = 5;
        $array_abc['b'][2] = 5;
        $array_abc['b'][3] = 5;
        $array_abc['b'][4] = 5;
        $array_abc['b'][5] = 5;
        $array_abc['b'][6] = 5;
        $array_abc['b'][7] = 5;
        $array_abc['b'][8] = 20000;
        $array_abc['c'][1] = 5;
        $array_abc['c'][2] = 5;
        $array_abc['c'][3] = 5;
        $array_abc['c'][4] = 5;
        $array_abc['c'][5] = 5;
        $array_abc['c'][6] = 5;
        $array_abc['c'][7] = 5;
        $array_abc['c'][8] = 20000;
        $array_abc['s'][1] = 5;
        $array_abc['s'][2] = 5;
        $array_abc['s'][3] = 5;
        $array_abc['s'][4] = 5;
        $array_abc['s'][5] = 5;
        $array_abc['s'][6] = 5;
        $array_abc['s'][7] = 5;
        $array_abc['s'][8] = 20000;
        $pdf = new FPDF();
        $pdf->AddPage();
        $pdf->AliasNbPages();
        $pdf->AddFont('sara', '', 'Sara.php');
        $pdf->AddFont('sarab', 'B', 'SaraB.php');
        $pdf->SetFont('sarab', 'B', 16);
        $pdf->Cell(25, 6, conv("ชื่อโครงการวิจัย"), 0, 0);
        $pdf->SetFont('sara', '', 16);
        $pdf->MultiCell(0, 6, conv($array_abc['object']['title']), 0, 1);
        $y1 = $pdf->GetY();
        $pdf->SetFont('sarab', 'B', 16);
        $pdf->Cell(125, 20, conv("ข้อรายการประเมิน"), 1, 0, 'C');
        $pdf->Cell(20, 20, conv("คะแนนเต็ม"), 1, 0, 'C');
        $pdf->Cell(45, 10, conv("คะแนนประเมิน"), 1, 1, 'C');
        $pdf->SetXY(155, $y1 + 10);
        $pdf->Cell(10, 10, conv("A"), 1, 0, 'C');
        $pdf->Cell(10, 10, conv("B"), 1, 0, 'C');
        $pdf->Cell(10, 10, conv("C"), 1, 0, 'C');
        $pdf->Cell(15, 10, conv("รวม"), 1, 0, 'C');
//1
        $pdf->Ln();
        $pdf->Cell(125, 10, conv("1. ชื่อโครงการ"), 'TRL', 0, 'L');
        $pdf->Ln();
        $pdf->SetFont('sara', '', 12);
        $pdf->Cell(125, 6, conv("สื่อความหมาย มีความกะทัดรัด และครอบคลุมครบถ้วน"), 'LRB', 0, 'L');
        $pdf->SetXY(135, $y1 + 20);
        $pdf->SetFont('sara', '', 16);
        $pdf->Cell(20, 16, conv("5"), 1, 0, 'C');
        $pdf->Cell(10, 16, conv($array_abc['a'][1]), 1, 0, 'C');
        $pdf->Cell(10, 16, conv($array_abc['b'][1]), 1, 0, 'C');
        $pdf->Cell(10, 16, conv($array_abc['c'][1]), 1, 0, 'C');
        $pdf->Cell(15, 16, conv($array_abc['s'][1]), 1, 0, 'C');
        $y1 = $pdf->GetY();
// 2
        $pdf->SetFont('sarab', 'B', 16);
        $pdf->Ln();
        $pdf->Cell(125, 10, conv("2. ความสำคัญ ที่มาของปัญหาที่ทำการวิจัย และการทบทวนเอกสารเกี่ยวข้อง"), 'TRL', 0, 'L');
        $pdf->Ln();
        $pdf->SetFont('sara', '', 12);
        $pdf->MultiCell(125, 6, conv("บอกความสำคัญและเหตุผลความจำเป็นที่ต้องทำวิจัยเรื่องนั้นอย่างชัดเจน มีการทบทวนตรวจสอบเอกสารความรู้ที่เกี่ยวข้องอย่างทันสมัยตลอดจนมีเอกสารอ้างอิงอย่างครอบคลุม และทันสมัย ความรู้ในเรื่องนั้นที่รู้แล้ว และมีส่วนใดที่ยังไม่รู้ จึงต้องการหาความรู้ใหม่โดยการทำวิจัย"), 'LRB');
        $y2 = $pdf->GetY() - $y1;
        $pdf->SetXY(135, $y1);
        $pdf->SetFont('sara', '', 16);
        $pdf->Cell(20, $y2, conv("10"), '1', 0, 'C');
        $pdf->Cell(10, $y2, conv($array_abc['a'][2]), 1, 0, 'C');
        $pdf->Cell(10, $y2, conv($array_abc['b'][2]), 1, 0, 'C');
        $pdf->Cell(10, $y2, conv($array_abc['c'][2]), 1, 0, 'C');
        $pdf->Cell(15, $y2, conv($array_abc['s'][2]), 1, 0, 'C');
        // 3
        $pdf->SetFont('sarab', 'B', 16);
        $pdf->Ln();
        $y3 = $pdf->GetY();
        $pdf->Cell(125, 10, conv("3. คำถามในการวิจัย (Research Question)"), 'TRL', 0, 'L');
        $pdf->Ln();
        $pdf->SetFont('sara', '', 12);
        $pdf->MultiCell(125, 6, conv("วัตถุประสงค์ของโครงการ หรือสมมุติฐานการวิจัย (Hypothesis) ชัดเจน และมีความจำเพาะ"), 'LRB', 'L');
        $y31 = $pdf->GetY() - $y3;
        $pdf->SetXY(135, $y3);
        $pdf->SetFont('sara', '', 16);
        $pdf->Cell(20, $y31, conv("10"), '1', 0, 'C');
        $pdf->Cell(10, $y31, conv($array_abc['a'][3]), 1, 0, 'C');
        $pdf->Cell(10, $y31, conv($array_abc['b'][3]), 1, 0, 'C');
        $pdf->Cell(10, $y31, conv($array_abc['c'][3]), 1, 0, 'C');
        $pdf->Cell(15, $y31, conv($array_abc['s'][3]), 1, 0, 'C');
        // 4
        $pdf->SetFont('sarab', 'B', 16);
        $pdf->Ln();
        $y4 = $pdf->GetY();
        $pdf->Cell(125, 10, conv("4. กระบวนวิธีวิจัย"), 'TRL', 0, 'L');
        $pdf->Ln();
        $pdf->SetFont('sara', '', 12);
        $pdf->MultiCell(125, 6, conv("มีรายละเอียดและความชัดเจนเพียงพอที่จะใช้ในการตรวจสอบติดตามผลได้ ใช้วิธีการที่ถูกต้อง และมีคุณภาพ มีขอบเขตของตัวอย่าง วิธีการเก็บตัวอย่าง หรือการวัดอย่างชัดเจน (ถ้าเก็บข้อมูลโดยใช้แบบสอบถาม ควรแนบตัวอย่างของแบบสอบถามมาด้วยจะได้คะแนนสูงกว่าไม่แนบ) มีวิธีวิเคราะห์หรือประมวลข้อมูล และวิธีการทางสถิติถูกต้อง ถ้าต้องการใช้ครุภัณฑ์หรือวัสดุที่มีลักษณะพิเศษควรระบุด้วย ถ้าเป็นงานวิจัยในลักษณะสิ่งประดิษฐ์หรือพัฒนา ให้บอกวิธีที่จะวัดประสิทธิภาพหรือสมรรถนะด้วย"), 'LRB', 'L');
        $y41 = $pdf->GetY() - $y4;
        $pdf->SetXY(135, $y4);
        $pdf->SetFont('sara', '', 16);
        $pdf->Cell(20, $y41, conv("10"), '1', 0, 'C');
        $pdf->Cell(10, $y41, conv($array_abc['a'][4]), 1, 0, 'C');
        $pdf->Cell(10, $y41, conv($array_abc['b'][4]), 1, 0, 'C');
        $pdf->Cell(10, $y41, conv($array_abc['c'][4]), 1, 0, 'C');
        $pdf->Cell(15, $y41, conv($array_abc['s'][4]), 1, 0, 'C');
        // 5
        $pdf->SetFont('sarab', 'B', 16);
        $pdf->Ln();
        $y5 = $pdf->GetY();
        $pdf->Cell(125, 10, conv("5. โอกาสที่จะทำงานชิ้นนี้ได้สำเร็จ"), 'TRL', 0, 'L');
        $pdf->Ln();
        $pdf->SetFont('sara', '', 12);
        $pdf->MultiCell(125, 6, conv("ขนาดของโครงการวิจัยมีความเหมาะสม และจะสามารถเก็บข้อมูลหรือดำเนินการได้ตามที่กำหนดไว้"), 'LRB', 'L');
        $y51 = $pdf->GetY() - $y5;
        $pdf->SetXY(135, $y5);
        $pdf->SetFont('sara', '', 16);
        $pdf->Cell(20, $y51, conv("5"), '1', 0, 'C');
        $pdf->Cell(10, $y51, conv($array_abc['a'][5]), 1, 0, 'C');
        $pdf->Cell(10, $y51, conv($array_abc['b'][5]), 1, 0, 'C');
        $pdf->Cell(10, $y51, conv($array_abc['c'][5]), 1, 0, 'C');
        $pdf->Cell(15, $y51, conv($array_abc['s'][5]), 1, 0, 'C');
        // 6
        $pdf->SetFont('sarab', 'B', 16);
        $pdf->Ln();
        $y6 = $pdf->GetY();
        $pdf->Cell(125, 10, conv("6. ความสำคัญหรือประโยชน์ที่จะได้จากโครงการวิจัย"), 'TRL', 0, 'L');
        $pdf->Ln();
        $pdf->SetFont('sara', '', 12);
        $pdf->MultiCell(125, 6, conv("ผลงานมีประโยชน์ต่อการประยุกต์แก้ปัญหา หรือเป็นลู่ทางสู่การวิจัย เพื่อแก้ปัญหาต่อไป หรือมีประโยชน์ต่อวงวิชาการหรือส่งเสริมการวิจัยในระดับบัณฑิตศึกษา หรือผลงานมีคุณภาพเพียงพอจะนำมาตีพิมพ์ในวารสารวิชาการได้"), 'LRB', 'L');
        $y61 = $pdf->GetY() - $y6;
        $pdf->SetXY(135, $y6);
        $pdf->SetFont('sara', '', 16);
        $pdf->Cell(20, $y61, conv("5"), '1', 0, 'C');
        $pdf->Cell(10, $y61, conv($array_abc['a'][6]), 1, 0, 'C');
        $pdf->Cell(10, $y61, conv($array_abc['b'][6]), 1, 0, 'C');
        $pdf->Cell(10, $y61, conv($array_abc['c'][6]), 1, 0, 'C');
        $pdf->Cell(15, $y61, conv($array_abc['s'][6]), 1, 0, 'C');
        // 6
        $pdf->SetFont('sarab', 'B', 16);
        $pdf->Ln();
        $y6 = $pdf->GetY();
        $pdf->Cell(125, 10, conv("7. งบประมาณ"), 'TRL', 0, 'L');
        $pdf->Ln();
        $pdf->SetFont('sara', '', 12);
        $pdf->MultiCell(125, 6, conv("สมเหตุสมผล มีความจำเป็นและเหมาะสม"), 'LRB', 'L');
        $y61 = $pdf->GetY() - $y6;
        $pdf->SetXY(135, $y6);
        $pdf->SetFont('sara', '', 16);
        $pdf->Cell(20, $y61, conv("5"), '1', 0, 'C');
        $pdf->Cell(10, $y61, conv($array_abc['a'][7]), 1, 0, 'C');
        $pdf->Cell(10, $y61, conv($array_abc['b'][7]), 1, 0, 'C');
        $pdf->Cell(10, $y61, conv($array_abc['c'][7]), 1, 0, 'C');
        $pdf->Cell(15, $y61, conv($array_abc['s'][7]), 1, 0, 'C');
        // 6
        $pdf->SetFont('sarab', 'B', 16);
        $pdf->Ln();
        $pdf->Cell(125, 6, conv("8. งบประมาณที่เหมาะสม (บาท)"), 'TRL', 0, 'L');
        $pdf->Cell(65, 6, conv(""), 'LR', 0);
        $pdf->Ln();
        $pdf->SetFont('sara', '', 16);
        $pdf->Cell(125, 6, conv("A"), 'LR', 0);
        $pdf->Cell(65, 6, conv($array_abc['a'][8]), 'LR', 0, 'R');
        $pdf->Ln();
        $pdf->Cell(125, 6, conv("B"), 'LR', 0);
        $pdf->Cell(65, 6, conv($array_abc['b'][8]), 'LR', 0, 'R');
        $pdf->Ln();
        $pdf->Cell(125, 6, conv("C"), 'LRB', 0);
        $pdf->Cell(65, 6, conv($array_abc['c'][8]), 'LRB', 0, 'R');
        $pdf->Ln();
        $pdf->Cell(125, 10, conv("คะแนนรวม"), 1, 0, 'L');
        $pdf->Ln();
        $pdf->Cell(125, 10, conv("คะแนนเฉลี่ย"), 1, 0, 'L');
        $pdf->Ln();
        $pdf->Cell(125, 10, conv("คะแนนร้อยละ"), 1, 0, 'L');
        $pdf->AddPage();
        $pdf->Cell(0, 10, conv("สรุปข้อข้อเสนอแนะจากผู้ทรงคุณวุฒิ"), 0, 1);
        $pdf->MultiCell(0, 10, conv("สรุปข้อข้อเสนอแนะจากผู้ทรงคุณวุฒิ"), 0);
        $pdf->Cell(0, 10, conv("ผลการประเมิน"), 0, 1);
        $pdf->Cell(0, 10, conv("สรุปข้อข้อเสนอแนะจากผู้ทรงคุณวุฒิ"), 0, 1);
        $pdf->Output();
    }

}
