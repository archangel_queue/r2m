<?php

class MyDb extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function GetLogIn($user, $pass) {
        $array = array($user, md5($pass));
        $str = "SELECT * FROM login AS ln WHERE ln.login_name=? AND ln.login_pass=?";
        $query = $this->db->query($str, $array);
        return $query->result();
    }

    public function GetRevs($rev = 0) {
        $str = empty($rev) ? "SELECT * FROM login AS ln WHERE ln.login_type='referee'" : "SELECT * FROM login AS ln WHERE ln.login_type='referee' AND ln.login_id=$rev";
        $query = $this->db->query($str);
        return $query->result();
    }

    public function GetFormA($group = 1) {
        $str = "SELECT * FROM bootcamp_a AS ba WHERE ba.a_group LIKE '$group' ORDER BY ba.a_group ASC, ba.a_code ASC";
        $query = $this->db->query($str);
        return $query->result();
    }

    public function GetFormB($group = 1) {
        $str = "SELECT * FROM bootcamp_b AS ba WHERE ba.a_group LIKE '$group' ORDER BY ba.a_group ASC, ba.a_code ASC";
        $query = $this->db->query($str);
        return $query->result();
    }

    public function GetPoints($team_id, $referee_id) {
        $str = "SELECT * FROM  `bootcamp_apoints` WHERE  `points_referee` = ? AND  `points_team` =?";
        $query = $this->db->query($str, array($referee_id, $team_id));
        $data = array();
        foreach ($query->result() as $row) {
            $data[$row->points_subject] = $row;
        }
        return $data;
    }

    public function GetPointsB($team_id, $referee_id) {
        $str = "SELECT * FROM  `bootcamp_bpoints` WHERE  `points_referee` = ? AND  `points_team` =?";
        $query = $this->db->query($str, array($referee_id, $team_id));
        $data = array();
        foreach ($query->result() as $row) {
            $data[$row->points_subject] = $row;
        }
        return $data;
    }

    public function GetPointsAll() {
        $str = "SELECT * FROM  `bootcamp_apoints`";
        $query = $this->db->query($str);
        $data = array();
        foreach ($query->result() as $row) {
            $data[$row->points_subject][$row->points_team][$row->points_referee] = $row;
        }
        return $data;
    }

    public function GetTeams($id) {
        $str = "SELECT * FROM teams AS ba WHERE ba.team_id LIKE '$id' ORDER BY team_name ASC";
        $query = $this->db->query($str);
        return $query->result();
    }

    public function GetTeamsID($id, $ward = "before") {
        if ($ward == "before") {
            $sign = "<";
            $sort = "DESC";
        } else {
            $sign = ">";
            $sort = "ASC";
        }
        $str = "SELECT team_id FROM teams AS ba WHERE ba.team_id $sign $id ORDER BY ba.team_id $sort LIMIT 0, 1";
        $query = $this->db->query($str);
        return $query->result();
    }

    public function GetTeamsIDFinal($id, $ward = "before") {
        if ($ward == "before") {
            $sign = "<";
            $sort = "DESC";
        } else {
            $sign = ">";
            $sort = "ASC";
        }
        $str = "SELECT team_id FROM teams AS ba "
                . "LEFT JOIN teamfilnal AS fi ON fi.f_team = ba.team_id "
                . "WHERE ba.team_name $sign '$id' AND fi.f_pass = 1 ORDER BY ba.team_name $sort LIMIT 0, 1";
        $query = $this->db->query($str);
        return $query->result();
    }

    public function GetTeamsJoinPoints($id, $rev_id = 0) {
        if (empty($rev_id))
            return;
        $str = "SELECT * FROM teams AS ba "
                . "LEFT JOIN (SELECT SUM(points_value) AS summ, points_team FROM `bootcamp_apoints` WHERE points_team LIKE '$id' AND points_referee IN (" . implode(',', $rev_id) . ") GROUP BY points_team) AS bt "
                . "ON bt.points_team=ba.team_id "
                . "LEFT JOIN teamfilnal AS fi ON fi.f_team = ba.team_id "
                . "WHERE ba.team_id LIKE '$id' ORDER BY team_name ASC";
        $query = $this->db->query($str);
        return $query->result();
    }

    public function GetTeamsJoinPointsAll($id) {
        $str = "SELECT * FROM teams AS ba "
                . "LEFT JOIN (SELECT SUM(points_value) AS summ, points_team FROM `bootcamp_apoints` WHERE points_team LIKE '$id' GROUP BY points_team) AS bt "
                . "ON bt.points_team=ba.team_id "
                . "LEFT JOIN teamfilnal AS fi ON fi.f_team = ba.team_id "
                . "WHERE ba.team_id LIKE '$id' ORDER BY team_name ASC";
        $query = $this->db->query($str);
        return $query->result();
    }

    public function GetTeamsJoinPointsFinish($id, $rev_id = 0) {
        if (empty($rev_id))
            return;
        $str = "SELECT * FROM teams AS ba "
                . "LEFT JOIN (SELECT SUM(points_value) AS sum1st, points_team FROM `bootcamp_apoints` WHERE points_team LIKE '$id' AND points_referee IN (" . implode(',', $rev_id) . ") GROUP BY points_team) AS bt "
                . "ON bt.points_team=ba.team_id "
                . "LEFT JOIN (SELECT SUM(points_value) AS sum2nd, points_team FROM `bootcamp_bpoints` WHERE points_team LIKE '$id' AND points_referee IN (" . implode(',', $rev_id) . ") GROUP BY points_team) AS bb "
                . "ON bb.points_team=ba.team_id "
                . "LEFT JOIN teamfilnal AS fi ON fi.f_team = ba.team_id "
                . "WHERE ba.team_id LIKE '$id' AND fi.f_pass = 1 ORDER BY team_name ASC";
        $query = $this->db->query($str);
        return $query->result();
    }

    public function GetLogins($type = 'referee', $id = 0) {
        $array = array('login_type' => $type);
        if (!empty($id))
            $this->db->where(array('login_id' => $id));
        $query = $this->db->get_where('login', $array);
        return $query->result();
    }

    public function UpdatePoints($data) {
        if (!empty($data)) {
            foreach ($data as $row) {
                $test = array('points_referee' => $row['points_referee'],
                    'points_team' => $row['points_team'],
                    'points_subject' => $row['points_subject'],
                );
                $up = array('points_value' => $row['points_value']);
                $query = $this->db->get_where('bootcamp_apoints', $test, 1);
                $exist = $query->result();
                if (empty($exist)) {
                    $this->db->insert('bootcamp_apoints', $row);
                } else {
                    $this->db->update('bootcamp_apoints', $up, $test);
                }
            }
        }
    }

    public function UpdatePointsB($data) {
        if (!empty($data)) {
            foreach ($data as $row) {
                $test = array('points_referee' => $row['points_referee'],
                    'points_team' => $row['points_team'],
                    'points_subject' => $row['points_subject'],
                );
                $up = array('points_value' => $row['points_value']);
                $query = $this->db->get_where('bootcamp_bpoints', $test, 1);
                $exist = $query->result();
                if (empty($exist)) {
                    $this->db->insert('bootcamp_bpoints', $row);
                } else {
                    $this->db->update('bootcamp_bpoints', $up, $test);
                }
            }
        }
    }

    public function UpdateLogin($login, $pass, $name = 0, $type = 0, $id = 0) {
        $array = array(
            'login_name' => $login,
        );
        $this->db->select('COUNT(*) AS c');
        $this->db->where($array);
        if (!empty($id))
            $this->db->where(array('login_id !=' => $id));
        $query = $this->db->get('login')->result()[0]->c;
        echo $this->db->last_query();
        if (!empty($name))
            $array += array('login_fullname' => $name);
        if (empty($id)) {
            $array['login_type'] = empty($type) ? 'referee' : $type;
            $pass = empty($pass) ? 123456 : $pass;
            $array['login_pass'] = md5($pass);
            if ($array['login_type'] != 'admin') {
                $json = json_encode(array('password' => $pass, 'create' => date('Y-m-d H:i:s')));
                $array += array('login_hints' => $json);
            }
            if ($query == 0) {
                $this->db->insert('login', $array);
                $this->session->set_flashdata('alert', array('text' => "เพิ่ม \"$login\" แล้ว", 'type' => 'success', 'head' => 'สำเร็จ'));
            } else {
                $this->session->set_flashdata('alert', array('text' => "\"$login\" มีอยู่แล้ว", 'type' => 'danger', 'head' => 'พบข้อผิดพลาด'));
            }
        } else {

            if (!empty($pass)) {
                $array['login_pass'] = md5($pass);
            }
            if (!empty($type))
                $array['login_type'] = $type;
            if ($array['login_type'] != 'admin') {
                $json = json_encode(array('password' => $pass, 'create' => date('Y-m-d H:i:s')));
                $array += array('login_hints' => $json);
            }
            if (empty($query))
                $this->db->update('login', $array, array('login_id' => $id));
        }
    }

    public function UpdateTeams($team_name, $team_id) {
        $array = array(
            'team_name' => $team_name,
        );
        if (empty($team_id)) {
            $this->db->select('COUNT(*) AS c');
            $this->db->where($array);
            $query = $this->db->get('teams')->result()[0]->c;
            if ($query == 0) {
                $this->db->insert('teams', $array);
                $this->session->set_flashdata('alert', array('text' => "เพิ่มทีม \"$team_name\" แล้ว", 'type' => 'success', 'head' => 'สำเร็จ'));
            } else {
                $this->session->set_flashdata('alert', array('text' => "ชื่อทีม \"$team_name\" มีอยู่แล้ว", 'type' => 'danger', 'head' => 'พบข้อผิดพลาด'));
            }
        } else {
            $this->db->update('teams', $array, array('team_id' => $team_id));
        }
    }

    public function RemoveTeams($team_id) {
        $success = $this->db->delete('teams', array('team_id' => $team_id));
        if (!empty($success)) {
            $this->session->set_flashdata('alert', array('text' => "ลบทีมแล้ว", 'type' => 'success', 'head' => 'สำเร็จ'));
        } else {
            $this->session->set_flashdata('alert', array('text' => "ลบทีมไม่สำเร็จ ทีมนี้อาจไม่มีอยู่ตั้งแต่ต้น", 'type' => 'danger', 'head' => 'พบข้อผิดพลาด'));
        }
    }

    public function UpdateTeamFinal($team_id, $pass) {
        $this->db->select('COUNT(*) AS c');
        $this->db->where($team_id);
        $count = $this->db->get('teamfilnal')->result()[0]->c;
        $complete = false;
        if (empty($count)) {
            $complete = $this->db->insert('teamfilnal', $team_id + array('f_pass' => $pass));
        } else {
            $complete = $this->db->update('teamfilnal', array('f_pass' => $pass), $team_id);
        }
        return json_encode(array('complete' => $complete, 'f_pass' => $pass) + $team_id);
    }

}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

