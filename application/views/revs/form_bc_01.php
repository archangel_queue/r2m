<ol class="breadcrumb">
    <li><a href="<?php echo site_url('/referee') ?>"><?php echo $this->session->userdata('login_fullname');?></a></li>
</ol>
<div class="row">
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-3 text-left"><h1><a href="<?php echo empty($before) ? 'javascript:void();' : site_url('/referee/scored1st/' . $before[0]->team_id) ?>" class="<?php echo empty($before) ? 'text-muted' : '' ?>"><span class="glyphicon glyphicon-backward"></span></a></h1></div>
                    <div class="col-xs-6 text-center">
                        <h1><?php echo $team[0]->team_name ?></h1>
                    </div>
                    <div class="col-xs-3 text-right"><h1><a href="<?php echo empty($after) ? 'javascript:void();' : site_url('/referee/scored1st/' . $after[0]->team_id) ?>" class="<?php echo empty($after) ? 'text-muted' : '' ?>""><span class="glyphicon glyphicon-forward"></span></a></h1></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 hidden-sm hidden-xs">
                <img src="<?php echo base_url('imgs/r2mlogo.jpg') ?>"  class="img-thumbnail img-responsive" />
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <form class="form-horizontal" role="form" method="post" id="form">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Opportunity Canvas</h3>
                </div>
                <div class="panel-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>หัวข้อประเมินโดยคณะกรรมการ</th>
                                <th>คะแนนเต็ม</th>
                                <th>คะแนน</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($elements as $element) { ?>
                                <?php if ($element->a_group == 1) { ?>
                                    <tr>
                                        <td><?php echo $element->a_name ?></td>
                                        <td><label id="max_<?php echo $element->a_code ?>"><?php echo $element->a_max ?></label></td>
                                        <td><input type="text" maxlength="4" class="form-control " id="input_<?php echo $element->a_code ?>" name="points[<?php echo $element->a_code ?>]" value="<?php echo!empty($points[$element->a_code]) ? $points[$element->a_code]->points_value : '' ?>"></td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">การนำเสนอ</h3>
                </div>
                <div class="panel-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>หัวข้อประเมินโดยคณะกรรมการ</th>
                                <th>คะแนนเต็ม</th>
                                <th class="text-center">คะแนน</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($elements as $element) { ?>
                                <?php if ($element->a_group == 2) { ?>
                                    <tr>
                                        <td><?php echo $element->a_name ?></td>
                                        <td><span id="max_<?php echo $element->a_code ?>"><?php echo $element->a_max ?></span></td>
                                        <td> <input  maxlength="4"  type="text" class="form-control "  id="input_<?php echo $element->a_code ?>" name="points[<?php echo $element->a_code ?>]"  value="<?php echo!empty($points[$element->a_code]) ? $points[$element->a_code]->points_value : '' ?>"></td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-default" name="submit" id="submit" value="1">บันทึกคะแนน</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $(':text').keyup(function () {
            if ($(this).val().length > 0) {
                var idmap = $(this).attr('id').replace(/[a-z|\_]+/g, "");
                var max = parseFloat($('#max_' + idmap).html());
                var cval = parseFloat($(this).val());
                if (cval <= max) {
                    $(this).parent().parent().removeClass('has-error');
                    $(this).focus();
                } else {
                    $(this).parent().parent().addClass('has-error');
                    $(this).focus();
                }
            }
        });
        $('#form').submit(function () {
            $(':text').each(function (i) {
                if ($(this).val().length > 0) {
                    var idmap = $(this).attr('id').replace(/[a-z|\_]+/g, "");
                    var max = parseFloat($('#max_' + idmap).html());
                    var cval = parseFloat($(this).val());
                    if (cval > max) {
                        $(this).parent().parent().addClass('has-error');
                        $(this).focus();
                        event.preventDefault();
                    }
                }
            });
        });
    });
</script>