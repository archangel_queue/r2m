<ol class="breadcrumb">
    <li><a href="<?php echo site_url('/content/finish') ?>"><?php echo $this->session->userdata('login_fullname'); ?></a></li>
</ol>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">ทีมที่ผ่านเข้ารอบสุดท้าย</h3>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-condensed table-bordered">
                <thead>
                    <tr>
                        <th>ชื่อทีม</th>
                        <th>รอบแรก</th>
                        <th>รอบที่สอง</th>
                        <th>&ensp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($teams as $team) { ?>
                        <tr>
                            <td><?php echo $team->team_name ?></td>
                            <td><?php echo (int) $team->sum1st ?></td>
                            <td><?php echo (int) $team->sum2nd ?></td>
                            <td><a href="<?php echo site_url('referee/scored2st/' . $team->team_id) ?>" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-star"></span> ให้คะแนน</a></td>                        
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>