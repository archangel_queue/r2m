<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <title>R2M2014</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="<?php echo base_url('css/bootstrap.css') ?>" />
        <link href="<?php echo base_url('font-awesome-4.1.0/css/font-awesome.min.css') ?>" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="<?php echo base_url('master.css') ?>" />
        <script src="<?php echo base_url('js/jquery-1.11.0.js') ?>"></script>
        <?php
        if (!empty($menu)) {
            ?>
            <style>
                body{
                    padding-top: 70px;
                }
            </style>
            <?php
        }
        ?>
    </head>
    <body>
        <?php echo $menu ?>
        <div class="container-fluid">
            <?php echo $content ?>
        </div>
        <script src="<?php echo base_url('js/bootstrap.min.js') ?>"></script>
    </body> 
</html>