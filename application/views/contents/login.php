<form class="form-signin" role="form" action="<?php echo site_url('/content/auth') ?>" method="post">
    <h2 class="form-signin-heading">Please sign in</h2>
    <input name="username" type="text" class="form-control" placeholder="" required autofocus>
    <input name="password" type="password" class="form-control" placeholder="" required>
    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
</form>

