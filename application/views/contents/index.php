<form class="form-horizontal" role="form">
    <div class="form-group">
        <label for="gimyong-id" class="col-sm-2 control-label">Gimyong ID</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="gimyong-id" placeholder="ใส่ไอดีของคนที่ต้องการสืบค้น">
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="button" id="gimyong-btn" class="btn btn-default">ค้น</button>
        </div>
    </div>
</form>
<div id="content-area">
    <table class="table table-condensed table-bordered">
        <thead>
            <tr>
                <th>ID</th>
                <th>NAME</th>
                <th>POST</th>
                <th>VIEWS</th>
                <th>REPLIES</th>
            </tr>
        </thead>
        <?php
        $month = 12;
        $year = date('Y');
        for ($y = $year; $y > 2009; $y--) {
            for ($m = $month; $m > 0; $m--) {
                ?>
                <tbody id="table-<?php echo $y ?>-<?php echo $m ?>" >
                    <tr class="danger"><th colspan="5"><?php echo $y ?>-<?php echo $m ?></th></tr>
                </tbody>
                <?php
            }
        }
        ?>
    </table>
</div>