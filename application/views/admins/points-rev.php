<ol class="breadcrumb">
    <li><a href="<?php echo site_url('/admin') ?>">Admin's Home</a></li>
    <li><a href="<?php echo site_url('/admin/referees') ?>">จัดการกรรมการ</a></li>
    <li class="active">ดูคะแนนรอบแรก <?php echo $revs[0]->login_fullname ?></li>
</ol>
<?php
$l = 0;
$th = 0;
$sd = 0;
$pref = 0;
$summary = array();
?>
<table class="table table-bordered table-striped table-hover">
    <thead>
        <tr>
            <th class="text-center"><?php echo $revs[0]->login_fullname ?></th>
            <?php foreach ($teams as $team) { ?>
            <?php $summary[$team->team_id] = 0?>
                <th  class="text-center"><?php echo $team->team_name ?></th>
            <?php } ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($topics as $topic) { ?>
            <?php
            if ($topic->a_group == 1) {
                ++$th;
                $pref = 1;
                $k = $th;
            } else {
                $pref = 2;
                ++$sd;
                $k = $sd;
            }
            ?>
            <tr>
                <th style="text-align: left"><?php echo sprintf("%d.%d ", $pref, $k); ?><?php echo $topic->a_name ?></th>
                <?php foreach ($teams as $team2) { ?>
                    <?php $point_sum = array() ?>
                    <td class="text-right">
                        <?php
                        $point_raw = isset($points[$topic->a_code][$team2->team_id][$revs[0]->login_id]) ? $points[$topic->a_code][$team2->team_id][$revs[0]->login_id]->points_value : NULL;
                        if (!is_null($point_raw)) {
                            array_push($point_sum, $point_raw);
                            $summary[$team2->team_id] += (double) $point_raw;
                            echo number_format($point_raw, 2);
                        } else {
                            echo '';
                        }
                        ?>
                    </td>
                <?php } ?>
            </tr>
        <?php } ?>
    </tbody>
    <tfoot>
        <tr>
            <th class="text-center">รวม</th>
            <?php foreach ($teams as $team) { ?>
                <th  class="text-right"><?php echo number_format($summary[$team->team_id], 2) ?></th>
                <?php } ?>
        </tr>
    </tfoot>
</table>