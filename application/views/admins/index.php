<div class="list-group">
    <a href="<?php echo site_url('admin/points')?>" class="list-group-item">
        <h4 class="list-group-item-heading">ดูคะแนน</h4>
        <p class="list-group-item-text">ดูผลการประเมินของคณะกรรมการ</p>
    </a>
    <a href="<?php echo site_url('admin/teams')?>" class="list-group-item">
        <h4 class="list-group-item-heading">จัดการทีมผู้เข้าแข่งขัน</h4>
        <p class="list-group-item-text">...</p>
    </a>
    <a href="<?php echo site_url('admin/referees')?>" class="list-group-item">
        <h4 class="list-group-item-heading">จัดการกรรมการ</h4>
        <p class="list-group-item-text">...</p>
    </a>
    <a href="<?php echo site_url('content/logout')?>" class="list-group-item">
        <h4 class="list-group-item-heading">ออกจากระบบ</h4>
        <p class="list-group-item-text">...</p>
    </a>
</div>