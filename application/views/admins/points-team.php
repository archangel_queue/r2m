<ol class="breadcrumb">
    <li><a href="<?php echo site_url('/admin') ?>">Admin's Home</a></li>
    <li><a href="<?php echo site_url('/admin/teams') ?>">จัดการทีม</a></li>
    <li class="active">ดูคะแนนรอบแรก ทีม<?php echo $teams[0]->team_name ?></li>
</ol>
<?php
$colspan = count($revs) + 2;
$l = 0;
$th = 0;
$sd = 0;
$pref = 0;
$summary = array();
$summary['avg'] = 0;
$summary['avgmn'] = 0;
?>
<table class="table table-bordered table-striped table-hover">
    <thead>
        <tr>
            <th class="text-center"><?php echo $teams[0]->team_name ?></th>
            <?php foreach ($teams as $team) { ?>
                <?php foreach ($revs as $rev2) { ?>
                    <?php
                    $summary[$rev2->login_id] = 0;
                    ?>
                    <th class="text-center"><?php echo $rev2->login_fullname ?></th>
                <?php } ?>
                <th class="text-center">Average</th>
                <th class="text-center">Average Without MinMax</th>
            <?php } ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($topics as $topic) { ?>
            <?php
            if ($topic->a_group == 1) {
                ++$th;
                $pref = 1;
                $k = $th;
            } else {
                $pref = 2;
                ++$sd;
                $k = $sd;
            }
            ?>
            <tr>
                <th style="text-align: left"><?php echo sprintf("%d.%d ", $pref, $k); ?><?php echo $topic->a_name ?></th>
                <?php foreach ($teams as $team2) { ?>
                    <?php $point_sum = array() ?>
                    <?php foreach ($revs as $rev) { ?>
                        <td class="text-right">
                            <?php
                            $point_raw = isset($points[$topic->a_code][$team2->team_id][$rev->login_id]) ? $points[$topic->a_code][$team2->team_id][$rev->login_id]->points_value : NULL;
                            if (!is_null($point_raw)) {
                                array_push($point_sum, $point_raw);
                                $summary[$rev->login_id] += (double) $point_raw;
                                echo number_format($point_raw, 2);
                            } else {
                                echo '';
                            }
                            ?>
                            <?php //echo sprintf("[%d][%d][%d]", $topic->a_code, $rev->login_id, $team2->team_id)   ?>
                        </td>
                    <?php } ?>
                    <td class="text-right">
                        <?php
                        $ssss = !empty($point_sum) ? number_format(array_sum($point_sum) / count($point_sum), 2) : '';
                        $summary['avg'] += $ssss;
                        echo $ssss;
                        ?>
                    </td>
                    <td class="text-right">
                        <?php
                        $sssx = remove_outliers($point_sum)['result'];
                        $summary['avgmn'] += $sssx;
                        echo $sssx;
                        ?>
                    </td>
                <?php } ?>
            </tr>
        <?php } ?>      
    </tbody>
    <tfoot>
        <tr>
            <th class="text-center">รวม</th>
            <?php foreach ($teams as $team) { ?>
                <?php foreach ($revs as $rev2) { ?>
                    <th  class="text-right"><?php echo number_format($summary[$rev2->login_id], 2) ?></th>
                <?php } ?>
                <th class="text-right"><?php echo number_format($summary['avg'], 2) ?></th>
                <th class="text-right"><?php echo number_format($summary['avgmn'], 2) ?></th>
                <?php } ?>
        </tr>
    </tfoot>
</table>