<ol class="breadcrumb">
    <li><a href="<?php echo site_url('/admin') ?>">Admin's Home</a></li>
    <li class="active">จัดการกรรมการ</li>
</ol>
<?php
$alert = $this->session->flashdata('alert');
if (!empty($alert)) {
    ?>
    <div class="alert alert-<?php echo!empty($alert['type']) ? $alert['type'] : 'info' ?> alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <?php echo!empty($alert['head']) ? '<strong>' . $alert['head'] . '!</strong> ' . $alert['text'] : $alert['text'] ?>
    </div>
    <?php
}
?>
<div class="panel panel-default">
    <div class="panel-heading">เพิ่มทีมใหม่</div>
    <div class="panel-body">
        <form role="form" method="post">
            <div class="form-group">
                <label for="referee-name">ชื่อกรรมการ</label>
                <input type="text" class="form-control input-sm" maxlength="32"id="referee-name" name="referee_name">
            </div>
            <div class="form-group">
                <label for="referee-user">ชื่อผู้ใช้</label>
                <input type="text" class="form-control input-sm" maxlength="32"id="referee-user" name="referee_user">
            </div>
            <div class="form-group">
                <label for="referee-pwd">รหัสผ่าน</label>
                <input type="password" class="form-control input-sm" maxlength="32"id="referee-pwd" name="referee_pwd">
            </div>
            <button type="submit" class="btn btn-default btn-sm">เพิ่มกรรมการ</button>
        </form>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">รายชื่อกรรมการ</div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-condensed table-bordered">
                <thead>
                    <tr>
                        <th class="text-center">ชื่อ-นามสกุล</th>
                        <th class="text-center" style="width: 100px">คะแนนรอบแรก</th>
                        <th class="text-center" style="width: 100px">คะแนนรอบสอง</th>
                        <th class="text-center">Username</th>
                        <th class="text-center">Password</th>
                        <th class="text-center">แก้ไข / ลบ</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($referees as $referee) {
                        $object = (object) array('password' => '');
                        if (!empty($referee->login_hints))
                            $object = json_decode($referee->login_hints);
                        ?>
                        <tr>
                            <td><?php echo $referee->login_fullname ?></td>
                            <td class="text-center"><a href="<?php echo site_url('admin/revs1st/' . $referee->login_id) ?>" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-star"></span></a></td>
                            <td class="text-center">-</td>
                            <td><?php echo $referee->login_name ?></td>
                            <td><?php echo $object->password ?></td>
                            <td class="text-center"><a href="<?php echo site_url('/admin/referee/' . $referee->login_id) ?>"class="btn btn-default btn-sm"><span class="glyphicon glyphicon-pencil"></span> แก้ไข</a> <a href="<?php echo site_url('/admin/referees/') ?>?login_id=<?php echo $referee->login_id ?>" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-trash"></span> ลบ</a></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>