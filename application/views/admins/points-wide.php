<ol class="breadcrumb">
    <li><a href="<?php echo site_url('/admin') ?>">Admin's Home</a></li>
    <li class="active">ดูคะแนนรอบแรก</li>
</ol>
<?php
$colspan = count($revs) + 2;
$l = 0;
$th = 0;
$sd = 0;
$pref = 0;
$arr = array();
?>
<div class="alert alert-info" role="alert">
    <?php
    foreach ($revs as $irev) {
        $arr[] = sprintf("%s = %s", $irev->login_id, $irev->login_fullname);
    }
    echo implode(", ", $arr);
    ?>
</div>
<table style="width: 100%" border="1">
    <thead>
        <tr>
            <th rowspan="2">&ensp;</th>
            <?php foreach ($teams as $team) { ?>
                <th colspan="<?php echo $colspan ?>"><?php echo $team->team_name ?></th>
            <?php } ?>
        </tr>
        <tr>
            <?php foreach ($teams as $team) { ?>
                <?php foreach ($revs as $rev2) { ?>
                    <th><?php echo $rev2->login_id ?></th>
                <?php } ?>
                <th>A</th>
                <th>M</th>
            <?php } ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($topics as $topic) { ?>
            <?php
            if ($topic->a_group == 1) {
                ++$th;
                $pref = 1;
                $k = $th;
            } else {
                $pref = 2;
                ++$sd;
                $k = $sd;
            }
            ?>

            <tr>
                <th style="text-align: left"><?php echo sprintf("%d.%d ", $pref, $k); ?><?php echo $topic->a_name ?></th>
                    <?php foreach ($teams as $team2) { ?>
                        <?php $point_sum = array() ?>
                        <?php foreach ($revs as $rev) { ?>
                        <td>
                            <?php
                            $point_raw = isset($points[$topic->a_code][$team2->team_id][$rev->login_id]) ? $points[$topic->a_code][$team2->team_id][$rev->login_id]->points_value : NULL;
                            if (!is_null($point_raw)) {
                                array_push($point_sum, $point_raw);
                                echo $point_raw;
                            } else {
                                echo '';
                            }
                            ?>
                            <?php //echo sprintf("[%d][%d][%d]", $topic->a_code, $rev->login_id, $team2->team_id)   ?>
                        </td>
                    <?php } ?>
                    <td><?php echo!empty($point_sum) ? number_format(array_sum($point_sum) / count($point_sum), 2) : ''; ?></td>
                    <td><?php echo remove_outliers($point_sum)['result'] ?></td>
                <?php } ?>
            </tr>
        <?php } ?>
    </tbody>
</table>