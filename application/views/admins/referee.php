<ol class="breadcrumb">
    <li><a href="<?php echo site_url('/admin') ?>">Admin's Home</a></li>
    <li><a href="<?php echo site_url('/admin/referees/') ?>">จัดการกรรมการ</a></li>
    <li class="active">แก้ไขข้อมูลการกรรมการ <?php echo $referee->login_name ?></li>
</ol>
<?php
$alert = $this->session->flashdata('alert');
if (!empty($alert)) {
    ?>
    <div class="alert alert-<?php echo!empty($alert['type']) ? $alert['type'] : 'info' ?> alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <?php echo!empty($alert['head']) ? '<strong>' . $alert['head'] . '!</strong> ' . $alert['text'] : $alert['text'] ?>
    </div>
    <?php
}
?>
<div class="panel panel-default">
    <div class="panel-heading">แก้ไขข้อมูล</div>
    <div class="panel-body">
        <form role="form" method="post">
            <div class="form-group">
                <label for="referee-name">ชื่อกรรมการ</label>
                <input type="text" class="form-control input-sm" maxlength="32" id="referee-name" name="referee_name" value="<?php echo $referee->login_fullname ?>">
            </div>
            <div class="form-group">
                <label for="referee-user">ชื่อผู้ใช้</label>
                <input type="text" class="form-control input-sm"  maxlength="32" id="referee-user" name="referee_user" value="<?php echo $referee->login_name ?>">
            </div>
            <?php
            $object = json_decode($referee->login_hints);
            ?>
            <div class="form-group">
                <label for="referee-pwd">รหัสผ่าน</label>
                <input type="password" class="form-control input-sm"  maxlength="32" id="referee-pwd" name="referee_pwd" value="<?php echo!empty($object->password) ? $object->password : '' ?>">
            </div>
            <button type="submit" class="btn btn-default btn-sm">แก้ไขข้อมูลกรรมการ</button>
        </form>
    </div>
</div>