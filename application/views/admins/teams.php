<ol class="breadcrumb">
    <li><a href="<?php echo site_url('/admin') ?>">Admin's Home</a></li>
    <li class="active">จัดการทีม</li>
</ol>
<?php
$alert = $this->session->flashdata('alert');
if (!empty($alert)) {
    ?>
    <div class="alert alert-<?php echo!empty($alert['type']) ? $alert['type'] : 'info' ?> alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <?php echo!empty($alert['head']) ? '<strong>' . $alert['head'] . '!</strong> ' . $alert['text'] : $alert['text'] ?>
    </div>
    <?php
}
?>
<?php
$second = true;
if (empty($second)) {
    ?>
    <div class="panel panel-default">
        <div class="panel-heading">เพิ่มทีมใหม่</div>
        <div class="panel-body">
            <form role="form" method="post">
                <div class="form-group">
                    <label for="team-name">ชื่อทีม</label>
                    <input type="text" class="form-control" id="team-name" name="team_name">
                </div>
                <button type="submit" class="btn btn-default">เพิ่มทีม</button>
            </form>
        </div>
    </div>
<?php } ?>
<div class="panel panel-default">
    <div class="panel-heading">ทีมที่เข้าร่วมการแข่งขัน</div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-condensed table-bordered">
                <thead>
                    <tr>
                        <th class="text-center">ชื่อทีม</th>
                        <th class="text-center">ดูคะแนนรอบแรก</th>
                        <th class="text-center">เข้ารอบสอง (<span id="passinto"></span>)</th>
                        <th class="text-center">ดูคะแนนรอบสอง</th>
                        <th class="text-center">แก้ไข / ลบ</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($teams as $team) { ?>
                        <tr>
                            <td><?php echo $team->team_name ?></td>
                            <td class="text-right"><?php if (!empty($team->summ)) { ?><a href="<?php echo site_url('/admin/round1st/' . $team->team_id) ?>"><?php echo number_format($team->summ, 2) ?></a><?php } else { ?><?php echo number_format($team->summ, 2) ?><?php } ?></td>
                            <td style="width: 10%" class="text-center"><input type="checkbox" name="pass" value="<?php echo $team->team_id ?>" <?php echo!empty($team->f_pass) ? 'checked' : '' ?> /></td>
                            <td class="text-right">-</td>
                            <td><button type="button" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-pencil"></span> แก้ไข</button> <a href="<?php echo site_url('/admin/teams/') ?>?team_id=<?php echo $team->team_id ?>" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-trash"></span> ลบ</a></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $('#passinto').html($(':checked').length);
        $(':checkbox').click(function () {
            var chk = $(this);
            $.post('<?php echo site_url('admin/json/') ?>', {team_id: $(this).val(), pass: ($(this).is(':checked') ? '1' : '0')}, function (data) {
                var jsonObj = $.parseJSON(data);
//                if (jsonObj.complete) {
//                    chk.parent().append('<p>' + jsonObj.f_pass + '</p>');
//                } else {
//                    chk.parent().append('<p>' + jsonObj.f_pass + '</p>');
//                }
                console.log(jsonObj);
                $('#passinto').html($(':checked').length);
            });
        });
    })
</script>