<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">R2M</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <?php
                $link = array(
                    'index' => 'หน้าหลัก',
                    'teams' => 'จัดการรายชื่อทีม',
                    'referees' => 'จัดการรายชื่อกรรมการ',
                    'points' => 'ดูคะแนนรอบแรก',
                );
                foreach ($link as $key => $value) {
                    ?> <li class="<?php echo ($this->router->method == $key) ? 'active' : '' ?>"><a href="<?php echo site_url('admin/' . $key) ?>"><?php echo $value ?></a></li><?php
                }
                ?>
                <li><a href="<?php echo site_url('content/logout') ?>">ออกจากระบบ</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
